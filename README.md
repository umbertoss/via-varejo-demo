# Via Varejo

- 1 Plataform 
-  AnyPoint Plataform
   - Design Center
        - Example - Customer API Visual Editor       
   
    - Exchange 
        - Conectores
        - Artefatos
        - Rest API : Appian API v1
        - Documentação 
        - Conectores
            - KAFKA
            - SalesForce
    - Customer API Visual Editor
     
    - API Manager
        - SFDC Proxy for Support API
    - Runtime Manager 
    - Visualizer
    - Monitoring



### Create API tshirt


#### Create Resource : order

Metodo: POST /order

REQUEST

| campo | type |
| --- | --- |
| size | stirng | 
| email  | stirng  |
|  name | stirng  | 
|  address1 | stirng  | 
|  address2 | stirng  | 
|  city | stirng  | 
|  stateOrProvince | stirng  | 
|  country | stirng  |  




`
{
	"size": "XG",
	"email": "mule@mulesoft.com",
	"name": "Mule MAX",
	"address1": "E 9TH ST",
	"address2": "STE C1285",
	"city": "Los Angeles",
	"stateOrProvince": "CA",
	"postalCode": "06037",
	"country": "EUA"
}`

RESPONSE
`
{
	"orderID": "XXX90"
}
`


#### Create Resource : order{orderid}

Metodo: GET /order{orderid}

QueryParam: email

RESPONSE
| campo | type |
| --- | --- |
| orderid | stirng | 
| status  | stirng  |
|  size | stirng  |  



`
{
	"orderid": "XXX",
	"status": "CREATE",
	"size": "HH"
}
`

### Mocking Service 
###  Publish Exchange 
### Scafold

#### Simular Mocking (API KIT)

http://localhost:8081/api/order

http://localhost:8081/api/order/1?email=mule@mule

1. Alterar API KIT 1.3.9

#### Implementar Flow 

1. Create Flow implementation
2. Create Flow
    - Transform
    - Consume
    - Transfor
2. Usar os metadados 
    - tshirt_input.json  
    - order_output.json
3. Utilizar Consume WebService
    - http://tshirts.demos.mulesoft.com/?wsdl
4. Deletar reference
5. Usar exemplo :
`
{"size":"L","email":"umbertoss1@gmail.com","name":"Marcelo teste 1","address1":"E 9TH ST","address2":"STE C1285","city":"Los Angeles","stateOrProvince":"CA","postalCode":"06037","country":"EUA"}`

Obs.: Alterar email para umbertossX@gmail.com